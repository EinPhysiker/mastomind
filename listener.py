from mastodon import Mastodon, StreamListener
from datetime import datetime, timezone
import json
import os.path
import re
import time
import secrets 

# class for all kinds of streaming 
class BotListener(StreamListener):

    separator = '-'

    # read current status from file if exists
    def __init__(self, status_file, mastodon):
        status_default = {
            # state can be NewWord or Running
            # count initializes as of 22.12.2023 20:45 
            "state" : "NewWord",
            "winner" : "",
            "word" : "",
            "found" : [],
            "guesses" : [],
            "results" : [],
            "last_guess" : {},
            "last_id" : None,
            "count" : 143,
            "top" : [1.4, "WASCHAKTIV"]
        }
        # read status and set defaults if needed
        # (in case new status entries are added)
        self.status = {}
        if os.path.isfile(status_file):
            with open("status.json") as status_json:
                self.status = json.load(status_json)
        for key in status_default.keys():
            if not key in self.status:
                self.status[key] = status_default[key]
        self.mastodon = mastodon
        self.max_delta = 600

    def print(self, *args, **kw):
        print("[%s]" % (datetime.now()),*args, **kw)    

    # write status to file
    # should be called after every status change
    def write_status(self):
        with open("status.json","w") as status_json:
            json.dump(self.status, status_json)

    # search for the first CAPITILIZED word with at least 5 letters
    # for ß also the lower case is allowed  
    def find_word(self, content, exclude=None):
        match = re.findall(r'\b[A-ZÖÄÜßẞ]+\b', content)
        word = None
        for candidate in match:
            if len(candidate) > 5 and candidate != exclude:
                word = candidate
                break
        if word:
            word = word.replace("ß","ẞ") 
        return word

    # processing new notifications
    def on_notification(self, notification):

        # only process mentions here 
        if notification["type"] != "mention":
            return
        message = notification["status"]

        # Some basic delay check
        send_date = message['created_at']
        arrival_date = datetime.now(timezone.utc)
        time_diff = arrival_date - send_date
        
        m_sender = message["account"]["acct"] 
        self.print( f"Delay for message from {m_sender}: {time_diff}")
        
        # only allow DMs for communication!
        if message["visibility"] != "direct":            
            # but check whether this might have been a guess
            guess = self.find_word(message["content"])
            if not guess:
                return
            if  self.status["state"] == "Running" \
                and ( guess in self.status["guesses"] or len(guess) != len(self.status["word"]) ): 
                return
            if self.status["state"] == "NewWord" \
                and message["account"]["acct"] != self.status["winner"]:
                return
            
            m_type = "Rateversuch"
            if self.status["state"] == "NewWord":
                m_type = "neues Wort"

            m_sender = message["account"]["acct"]
            self.print( f"Possible non-DM guess/suggestion from {m_sender}: {guess}")

            m_id = message["id"]
            self.mastodon.status_post(
                f"@{m_sender}\nInfo: Wenn das als {m_type} gedacht war, bitte als DM senden!",
                in_reply_to_id = m_id,
                visibility = "direct"
            )
            return

        if "Status" in message["content"]:
            m_id = message["id"]
            self.mastodon.status_post(
                f"@{m_sender}\nDelay Info:\n\n"
                f"Sent: {send_date.astimezone()}\nReceived: {arrival_date.astimezone()}\nDelay: {time_diff}",
                in_reply_to_id = m_id,
                visibility = "direct"
            )
            return

        # processing DMs
        if self.status["state"] == "NewWord":
            # check for new word
            self.process_new_word(message)
        elif self.status["state"] == "Running":
            # process guesses 
            self.process_guesses(message)

    def process_new_word(self, message):
        # some info 
        m_sender = message["account"]["acct"]
        m_id = message["id"]
        m_visibility = message["visibility"]
        m_content = message["content"]

        # only accepted from winner 
        if m_sender == self.status["winner"]:
            new_word = self.find_word(m_content)
            if not new_word:
                self.print(f"No new word found from @{m_sender}: {m_content}")
                self.mastodon.status_post(
                    f"@{m_sender}\n\nIch kann in Deiner Nachricht kein gültiges Wort finden.",
                    in_reply_to_id = m_id, 
                    visibility = "direct"
                )
                return
            # there was an easier way to do that ...
            template = ""
            for x in new_word:
                template += BotListener.separator + " "
            self.status["count"] += 1
            count = self.status["count"]
            res = self.mastodon.status_post(
                f"Runde {count} #MastoMind, diesmal von @{m_sender}!\n\n{template} ({len(new_word)})\n\n"
                f"Rateversuche per DM an mich. Das erste Wort in GROßBUCHSTABEN wird als Vorschlag genommen. Zum Mitspielen bitte mir folgen. (Und bitte keine Spezialseiten zum Lösen von Buchstabenrätseln nutzen.)",
                visibility="public")
                # public
            self.print(f"Runde {count}")
            self.print(f"New word from {m_sender}: {new_word}")
            self.status["word"] = new_word
            self.status["found"] = [ BotListener.separator for x in new_word ]
            self.status["state"] = "Running"
            self.status["guesses"] = []
            self.status["results"] = []
            self.status["last_guess"] = {}
            if "id" in res:
                self.status["last_id"] = res["id"]
            self.write_status()

    def process_guesses(self, message):
        # some info 
        m_sender = message["account"]["acct"]
        m_id = message["id"]
        m_visibility = message["visibility"]
        m_content = message["content"]
        last_winner = self.status["winner"]

        # find guess word
        guess = self.find_word(m_content)
        if not guess:
            self.print(f"No word found from @{m_sender}: {m_content}")
            return

        # first check if guess is correct 
        if guess == self.status["word"]:
            score_full = (len(self.status["guesses"])+1) / len(guess)
            score = round(score_full, 2)
            top_score = self.status["top"][0]
            top_word = self.status["top"][1]
            top_status = "is"
            if top_score < score:
                self.status["top"] = [score, guess]
            self.mastodon.status_post(f"Herzlichen Glückwunsch @{m_sender}!\nDu hast das Rätsel von @{last_winner} gelöst:\n{guess}"
                                      f"\n\nBU-Score: {score:.2f}\nBestes Wort bisher: {top_word} mit {top_score:.2f}."
                                      f"\n\nDu darfst mir für die nächste Runde #MastoMind per DM ein neues Wort schicken (in GROßBUCHSTABEN).",
                                in_reply_to_id=self.status["last_id"], visibility="public")
                                # public
            self.print("Guessed!")
            self.status["state"] = "NewWord"
            self.status["winner"] = m_sender
            self.status["word"] = ""
            self.status["found"] = []
            self.status["guesses"] = []
            self.status["results"] = []
            self.status["last_guess"] = {}
            self.status["last_id"] = None
            self.write_status()
            return 
 
        # check time-out 
        # simply ignore guesses until then
        if m_sender in self.status["last_guess"]:
            delta = time.time() - self.status["last_guess"][m_sender]
            if delta < self.max_delta: 
                self.print(f"{m_sender} time limit: {delta}")
                self.mastodon.status_post(
                    f"@{m_sender}\n\nDenke an den {int(self.max_delta/60)} Minuten Time-out zwischen den Versuchen: noch {int(self.max_delta-delta)} s",
                    in_reply_to_id = m_id, 
                    visibility = "direct"
                )
                return

        # if in guessed list, notify sender per DM
        if guess in self.status["guesses"]:
            self.print(f"Already guessed: {guess}")
            self.mastodon.status_post(
                f"@{m_sender}\nInfo: {guess} wurde schon versucht.",
                in_reply_to_id = m_id,
                visibility = "direct"
            )
            return
        # if word has wrong length, notify sender per DM
        if len(guess) != len(self.status["word"]):
            len_guess = len(guess)
            len_word = len(self.status["word"])
            self.print(f"Wrong length: {guess}")
            self.mastodon.status_post(
                f"@{m_sender}\nInfo: {guess} hat die falsche Länge ({len_guess} statt {len_word}).",
                in_reply_to_id = m_id,
                visibility = "direct"
            )
            return
        # check consistency (only testing for the moment)
        ### self.check_consistency(guess, m_sender)
        # process regular guess
        black = 0
        white = 0
        right_letters = ""
        left_in_guess = []
        left_in_word = []
        # search exactly matching letters
        # if letter is correct at the right place add to right_letters
        # if not, add in lower case and save the remaining letters
        for i in range(len(guess)):
            if guess[i] == self.status["word"][i]:
                right_letters = right_letters + guess[i]
                self.status["found"][i] = guess[i]
                black += 1
            else:
                right_letters = right_letters + guess[i].lower()
                left_in_guess.append(guess[i])
                left_in_word.append(self.status["word"][i])
        # now count the matching letters at the wrong place
        # delete matching letters from the list
        for i in range(len(left_in_guess)):
            if left_in_guess[i] in left_in_word:
                pos = left_in_word.index(left_in_guess[i])
                left_in_word[pos] = "."
                white += 1
        self.print(f"Result for {guess}: black {black}, white {white}, matching {right_letters}")

        mask = ""
        for c in self.status["found"]:
            mask += c + " "
        answer = f"Aktuelle Runde von @{last_winner}, @{m_sender} hat getippt:\n{right_letters} + {white} Bst.\n"
        answer += f"\n{mask} ({len(guess)})\n\n"
        if self.status["results"]:
            answer += "\nLetzte 5 Tips:\n"
        n = 0
        for result in reversed(self.status["results"]):
            answer += result
            answer += "\n"
            n += 1
            if n >= 5:
                break
        answer += "\n#MastoMind"        
        res = self.mastodon.status_post(answer,
                            in_reply_to_id=self.status["last_id"], visibility="unlisted")
                            # unlisted            
        self.status["guesses"].append(guess)
        self.status["results"].append(f"{right_letters} + {white} Bst.")
        self.status["last_guess"][m_sender] = time.time()
        if "id" in res:
            self.status["last_id"] = res["id"]
        self.write_status()
        return
    
    def check_consistency(self, guess, m_sender):
        # check whether word is consistent with found letters
        consistent_black = True
        for i in range(len(guess)):
            if self.status["found"][i] != BotListener.separator and self.status["found"][i] != guess[i]:
                consistent_black = False 
                break            
        # check whether word is consistent with excluded letters
        consistent_white = True
        inconsistency = None
        for result in self.status["results"]:
            for i in range(len(guess)):
                if guess[i].lower() == result[i] and guess[i] != self.status["found"][i]:
                    consistent_white = False
                    inconsistency = result[:len(guess)]
                    break
            if not consistent_white:
                break
        if consistent_black and consistent_white:
            return True
        # print info
        answer = f"@{m_sender}\n\n"
        if not consistent_black:
            mask = ""
            for c in self.status["found"]:
                mask += c + " "
            answer += f"{guess} is inconsistent with pattern {mask}\n\n"
        if not consistent_white:
            answer += f"{guess} is inconsistent with result of guess {inconsistency}" 
        self.print(answer)
        return False
