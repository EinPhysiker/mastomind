from mastodon import Mastodon, StreamListener
import secrets
from listener import BotListener
import time
import traceback

import requests

# Define the ntfy server URL and topic
ntfy_url = secrets.ntfy_url

mastodon = Mastodon(access_token=secrets.token, api_base_url=secrets.instance)
listener = BotListener("status.json", mastodon)

print("Status:", listener.status)
print("Here we go ...")
# Quick solution for the moment 
last_start = 0
num_tries = 0
while True:
    error_message = ""
    try:
        print("Start streaming")
        last_start = time.time()
        mastodon.stream_user(listener)
    except Exception as e:
        if time.time() - last_start < 60:
            num_tries += 1
            if num_tries > 10:
                print("Mastomind: to many restarts, giving up ...")
                break
        else:
            num_tries = 0

        exception_type = type(e).__name__
        exception_args = e.args
        exception_traceback = traceback.format_exc()

        print(secrets.instance)
        print("Exception type:", exception_type)
        print("Exception arguments:", exception_args)
        #print("Exception traceback:", exception_traceback)
        #print("Streaming interrupted:", e)
        error_message = f"Mastomind: {e}"

    try:
        requests.post(ntfy_url, data=error_message)
    except Exception as e:
        print(f"Sending ntfy message failed: {e}")
        
    time.sleep(30)






