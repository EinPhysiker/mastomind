# ToDo

## Likely
* if there are no guesses, post a reminder afer some time; if there are still no guesses, close the round and open the suggestions for new words for all
* if the winner does not send a new word, open the suggestions for all 

## Unlikely
* block guesses from the original sender (?)
* protect against server shutdowns
* send hints after basic checks (ignoring right/wrong letters) - 

