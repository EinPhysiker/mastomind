# MastoMind

## Idee

Ein Bot, der per DM ein zu spielendes Wort entgegen nimmt, auf Rateversuche reagiert und diese nach vorgegebenen Regeln bewertet. Der/die Sieger:in darf ein neues Wort vorschlagen.

Nach einem Vorschlag von @joschtl@chaos.social

## Ablauf 
### Neues Wort
Dem Bot wird vom Gewinner/von der Gewinnerin der letzten Runde per DM ein zu spielendes Wort mitgeteilt. Das erste großgeschriebe Wort mit mehr als 5 Buchstaben in der DM wird als neues Wort genommen. Buchstaben sind A-Z, Ä, Ö, Ü und das (große wie kleine) ß. Der Bot kündigt dies dann öffentlich ("public") mit dem Hashtag \#MastoMind an.

### Rateversuche
Spieler können Wortvorschläge per DM ("direkt", "private Nachricht", "nur erwähnte Profile") an den Bot schicken. Der Bot bewertet diese nach folgenden Kriterien:
* die Buchstaben, die richtig und an der richtigen Stelle sind, werden markiert
* andere Buchstaben, die zwar vorkommen, aber an der falschen Stelle sind, werden gezählt

Der Bot antwortet "unlisted" mit dem Ergebnis. Der Rateversuch wird genannt, wobei Buchstaben, die an der richtigen Stelle sind, groß sind, die anderen klein. Außerdem gibt es die Zahl der Buchstaben, die richtig, aber an der falschen Stelle sind. Zusätzlich gibt es ein Pattern mit den bisher gefundenen Buchstaben aus und die letzten 5 Rateversuche.

Um über die Antwort benachrichtigt zu werden, muss man dem Bot folgen. Vorschlag: man legt eine Liste an, die nur den Bot enthält. Das hat den gleichen Effekt, wie bisher das Folgen des Hashtags.

### Spielende
Wenn das Wort geraten wurde, verkündet der Bot das wieder öffentlich mit Hashtag. Ein Score "Anzahl Versuche / Anzahl Buchstaben" wird nach einem Vorschlag von @bitsunited@chaos.social ausgegeben. Außerdem wird das Wort mit dem bisher höchsten Score angezeigt (und bei Bedarf dann ausgetauscht). Bei Gleichstand bleibt das frühere.

## Hinweise

* Alle Nachrichten an den Bot, die nicht DMs sind, werden ignoriert. Ist ein passendes Wort in der Nachricht, schickt der Bot einen Hinweis als DM.
* Zwischen zwei Rateversuchen von der gleichen Person müssen 10 Minuten vergehen. Wenn das nicht eingehalten wird, schickt der Bot eine Erinnerung per DM. Ausnahme: Lösen kann man zu jeder Zeit.
* Ankündigung, Bewertungen und Auflösung werden als ein Thread gepostet
* Wenn das Wort nicht die richtige Länge hat oder schon mal geraten wurde, teilt der Bot das per DM mit.
* Zum Testen: Ein minimaler Konsistenzcheck für die Rateversuche werden als Log ausgegeben 

## Usage

Copy the file "secrets_sample.py" to "secrets.py" and enter the correct access token and instance. You get the token for your account in Settings/Development. Run mastomind.py.

The code might also serve as hint on how to build a bot listening on certain streams. 

